#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [[ -z ${SCALEWAY_PROFILE} || -z ${SCALEWAY_K8S_VERSION} || -z ${SCALEWAY_K8S_CNI} ]]; then
  echo 'Missing at least one of the following variables:'
  echo '* SCALEWAY_PROFILE'
  echo '* SCALEWAY_K8S_VERSION'
  echo '* SCALEWAY_K8S_CNI'
  exit 1
fi

scw -p ${SCALEWAY_PROFILE} k8s cluster create \
  name=demo-k8s-basics \
  version=${SCALEWAY_K8S_VERSION} \
  cni=${SCALEWAY_K8S_CNI} \
  pools.0.size=2 \
  pools.0.node-type=DEV1-M \
  pools.0.name=demo-pool \
  enable-dashboard=true

export CLUSTER_ID=$(scw -p ${SCALEWAY_PROFILE} k8s cluster list \
  name=demo-k8s-basics \
  | grep -Eo "^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}" \
)

echo ""
echo "Waiting for cluster to be up and running..."
scw -p ${SCALEWAY_PROFILE} k8s cluster wait ${CLUSTER_ID}

scw -p ${SCALEWAY_PROFILE} k8s kubeconfig install ${CLUSTER_ID}

export SCALEWAY_CONTEXT="admin@demo-k8s-basics-${CLUSTER_ID}"

echo ""
echo " ✓ Cluster set up and ready to receive traffic"

