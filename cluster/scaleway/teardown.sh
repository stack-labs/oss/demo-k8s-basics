#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [[ -z ${SCALEWAY_PROFILE} ]]; then
  echo 'Missing at least one of the following variables:'
  echo '* SCALEWAY_PROFILE'
  exit 1
fi

export CLUSTER_ID=$(scw -p ${SCALEWAY_PROFILE} k8s cluster list \
  name=demo-k8s-basics \
  | grep -Eo "^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}" \
)

scw -p ${SCALEWAY_PROFILE} k8s kubeconfig uninstall ${CLUSTER_ID}
scw -p ${SCALEWAY_PROFILE} k8s cluster delete ${CLUSTER_ID}
