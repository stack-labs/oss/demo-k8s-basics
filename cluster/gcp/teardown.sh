#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [[ -z ${GOOGLE_PROJECT_ID} || -z ${GOOGLE_CLUSTER_LOCATION} ]]; then
  echo 'Missing at least one of the following variables:'
  echo '* GOOGLE_PROJECT_ID'
  echo '* GOOGLE_CLUSTER_LOCATION'
  exit 1
fi

gcloud config set project "${GOOGLE_PROJECT_ID}"

gcloud beta container \
  --project "${GOOGLE_PROJECT_ID}" \
  clusters delete "demo-k8s-basics" \
  --zone "${GOOGLE_CLUSTER_LOCATION}" \
  --quiet
