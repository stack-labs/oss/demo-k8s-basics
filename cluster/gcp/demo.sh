#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl config use-context "${GOOGLE_CONTEXT}"

kubectl apply -f $HERE/../app/app.yml

echo ""
echo "Waiting for demo to be up and running..."
kubectl wait --namespace demo \
  --for=condition=ready pod \
  --selector=app=server \
  --timeout=60s

export CLUSTER_IP=$(kubectl --namespace demo get service/demo-service -o json | jq '.status.loadBalancer.ingress[0].ip' -r)
echo ""
echo " ✓ Demo running at http://${CLUSTER_IP}"