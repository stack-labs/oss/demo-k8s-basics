#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Create GCP cluster
if [[ -z ${GOOGLE_PROJECT_ID} || -z ${GOOGLE_CLUSTER_LOCATION} ]]; then
  echo 'Missing at least one of the following variables:'
  echo '* GOOGLE_PROJECT_ID'
  echo '* GOOGLE_CLUSTER_LOCATION'
  exit 1
fi

gcloud config set project "${GOOGLE_PROJECT_ID}"

echo ""
echo "Waiting for container API to be enabled..."
gcloud services enable container.googleapis.com
echo " ✓ Container API activated"

echo ""
echo "Waiting for network API to be enabled..."
gcloud services enable servicenetworking.googleapis.com
echo " ✓ Network API activated"

gcloud beta container \
  --project "${GOOGLE_PROJECT_ID}" \
  clusters create "demo-k8s-basics" \
  --zone "${GOOGLE_CLUSTER_LOCATION}" \
  --release-channel "regular" \
  --machine-type "g1-small" \
  --image-type "COS" \
  --disk-type "pd-standard" \
  --disk-size "10" \
  --preemptible \
  --num-nodes "3" \
  --enable-stackdriver-kubernetes \
  --enable-ip-alias \
  --addons HorizontalPodAutoscaling,HttpLoadBalancing \
  --enable-autoupgrade \
  --enable-autorepair \
  --create-subnetwork "name=demo-k8s-basics" 

echo ""
echo " ✓ Cluster set up and ready to receive traffic"

gcloud container clusters \
  get-credentials "demo-k8s-basics" \
  --zone "${GOOGLE_CLUSTER_LOCATION}" \
  --project "${GOOGLE_PROJECT_ID}"

export GOOGLE_CONTEXT="gke_${GOOGLE_PROJECT_ID}_${GOOGLE_CLUSTER_LOCATION}_demo-k8s-basics"