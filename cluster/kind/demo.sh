#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Switch context
kubectl config use-context "${KIND_CONTEXT}"

# Deploy application
kubectl apply -f $HERE/../app/app.yml

echo ""
echo "Waiting for demo to be up and running..."

kubectl wait --namespace demo \
  --for=condition=ready pod \
  --selector=app=server \
  --timeout=60s

# Configure ingress
kubectl apply -f $HERE/ingress.yml

# Output server location
echo ""
echo " ✓ Demo running on http://127.0.0.1"