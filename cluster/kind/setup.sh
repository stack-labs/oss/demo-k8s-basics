#!/usr/bin/env bash
set -e

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Create KinD cluster
kind create cluster --config=$HERE/kind.config.yml --name=demo-k8s-basics
kubectl cluster-info --context kind-demo-k8s-basics

export KIND_CONTEXT="kind-demo-k8s-basics"

# Set context
kubectl config use-context "${KIND_CONTEXT}"

# Deploy nginx ingress controller
kubectl apply -f $HERE/nginx.controller.yml

# Wait for the order to be processed by K8S
echo ""
echo "Waiting for the ingress controller to be up and running ..."
sleep 25

# Wait for the controller to be deployed
kubectl wait --namespace ingress-nginx \
  --context=kind-demo-k8s-basics \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s

echo ""
echo " ✓ Cluster set up and ready to receive traffic"