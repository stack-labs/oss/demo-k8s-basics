const DELAY = 1500;
const TIMEOUT = 5000;

function sendCar(metadata) {

    return new Promise(function(resolve) {
        const id = 'airplane-' + Date.now().toString();
        const airplane = document.createElement("div");
        airplane.setAttribute('id', id);
        airplane.classList.add('c-plane');
        airplane.innerHTML = `
      <div style="transform: scale(0.5)">
        <div class="c-plane__body">
          <ul class="c-plane__portholes c-portholes">
            <li class="c-plane__porthole"></li>
            <li class="c-plane__porthole"></li>
            <li class="c-plane__porthole"></li>
            <li class="c-plane__porthole"></li>
          </ul>
          <div class="c-plane-main-wing" style="background:${metadata.color}; color:${metadata.color}"></div>
          <div class="c-plane__forebody">
            <div class="c-plane__cockpit">
              <div class="c-plane__cockpit-glass"></div>
            </div>
            <div class="c-plane__nose"></div>
          </div>
          <div class="c-plane__stern">
            <div class="c-plane__tail">
              <div class="c-plane__tail-top" style="background:${metadata.color}"></div>
              <div class="c-plane__tail-bottom" style="background:${metadata.color}"></div>
              <div class="c-plane-turbine"></div>
              <div class="c-plane__stern-wing" style="background:${metadata.color}"></div>
            </div>
          </div>
        </div>
      </div>
    `

        const road = document.getElementById('way');
        road.appendChild(airplane);

        new mojs.Html({
            el: '#' + id,
            x: {
                '-40vw': '140vw',
                duration: 4000,
                easing: mojs.easing.linear,
                onComplete() {
                    // the car reached the other side
                    road.removeChild(airplane);
                }
            },
            y: {
                '50vh': `${45 + Math.ceil(Math.floor(Math.random() * 15) - 15)}vh`,
                duration: Math.ceil(Math.floor(Math.random() * 3000) + 1000),
                repeat: 5,
                easing: mojs.easing.ease,
                isYoyo: true,
            }
        }).play();

        resolve();
    })
}

function parseResponse(response) {
    return response.json();
}

function query(url) {
    return Promise.race([
        fetch(url),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('timeout')), TIMEOUT)
        )
    ])
}

function run() {
    return query('/echolor')
        .then(parseResponse)
        .then(sendCar)
        .finally(function() {
            setTimeout(run, DELAY);
        })
}


// infinite loop
run();