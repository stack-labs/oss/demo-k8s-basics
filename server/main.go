package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"demo-k8s-server/_log"
	"demo-k8s-server/configuration"
	"demo-k8s-server/handlers"

	"github.com/gorilla/mux"
)

var (
	config = new(configuration.Configuration)
	logger _log.Logger
	router *mux.Router
)

func init() {
	exitOnError(config.FromEnvironment())
	logger = _log.New(config.LoggingLevel)

	router = mux.NewRouter()
}

func main() {
	go func() {
		logger.Info(context.Background(), "health handler listening", _log.Fields{"port": config.HealthPort})
		exitOnError(http.ListenAndServe(fmt.Sprintf(":%d", config.HealthPort), handlers.NewHealth()))
	}()

	router.Handle("/echolor", handlers.NewEcholor(config, logger))
	router.Handle("/busy", handlers.NewBusy(config))
	router.PathPrefix("/").Handler(handlers.NewStatic(config))

	logger.Info(context.Background(), "main handler listening", _log.Fields{"port": config.Port})
	exitOnError(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), router))
}

func exitOnError(err error) {
	if err == nil {
		return
	}
	if logger == nil {
		fmt.Println(err)
		os.Exit(1)
		return
	}
	logger.Fatal(context.Background(), "failed during initialization", _log.Fields{"error": err})
}
