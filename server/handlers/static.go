package handlers

import (
	"net/http"

	"demo-k8s-server/configuration"
)

func NewStatic(config *configuration.Configuration) http.Handler {
	return http.FileServer(http.Dir(config.StaticDirectory))
}
