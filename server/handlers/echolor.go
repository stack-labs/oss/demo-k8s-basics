package handlers

import (
	"encoding/json"
	"net/http"
	"time"

	"demo-k8s-server/_log"
	"demo-k8s-server/configuration"
)

func NewEcholor(config *configuration.Configuration, logger _log.Logger) http.Handler {
	return &echolor{
		config: config,
		logger: logger,
	}
}

type echolor struct {
	config *configuration.Configuration
	logger _log.Logger
}

func (e *echolor) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	ctx := request.Context()
	e.logger.Info(ctx, "request received", _log.Fields{"headers": request.Header, "host": request.Host, "url": request.URL})

	time.Sleep(e.config.Delay)

	response := indexResponse{
		Delay: e.config.Delay.Milliseconds(),
		Color: e.config.Color,
	}
	body, err := json.Marshal(response)
	if err != nil {
		e.logger.Error(ctx, "failed to marshall response", _log.Fields{"response": response})
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	header := writer.Header()
	header.Add("Content-Type", "application/json")

	writer.WriteHeader(http.StatusOK)

	if _, err = writer.Write(body); err != nil {
		e.logger.Error(ctx, "failed to write body", _log.Fields{"body": string(body)})
	}
}

type indexResponse struct {
	Delay int64  `json:"delay"`
	Color string `json:"color"`
}
