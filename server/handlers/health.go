package handlers

import (
	"net/http"
)

func NewHealth() http.Handler {
	return new(health)
}

type health struct{}

func (*health) ServeHTTP(writer http.ResponseWriter, _ *http.Request) {
	writer.WriteHeader(http.StatusOK)
}
