package handlers

import (
	"fmt"
	"math"
	"net/http"

	"demo-k8s-server/configuration"
)

func NewBusy(config *configuration.Configuration) http.Handler {
	return &busy{
		config: config,
	}
}

type busy struct {
	config *configuration.Configuration
}

func (b *busy) ServeHTTP(writer http.ResponseWriter, _ *http.Request) {
	x := 0.0001
	for i := int64(0); i <= b.config.BusyLoopIterations; i++ {
		x = math.Sqrt(x)
	}
	writer.WriteHeader(http.StatusOK)
	_, _ = fmt.Fprint(writer, x)
}
