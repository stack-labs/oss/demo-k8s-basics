package configuration

import (
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

type Configuration struct {
	LoggingLevel       string        `default:"DEBUG" split_words:"true"`
	Port               int           `default:"8080" split_words:"true"`
	HealthPort         int           `default:"8181" split_words:"true"`
	Color              string        `default:"white" split_words:"true"`
	Delay              time.Duration `default:"50ms" split_words:"true"`
	BusyLoopIterations int64         `default:"1000000000" split_words:"true"`
	StaticDirectory    string        `default:"assets" split_words:"true"`
}

func (c *Configuration) FromEnvironment() error {
	if err := envconfig.Process("APP", c); err != nil {
		return errors.Wrap(err, "failed to load configuration from environment")
	}
	return nil
}
