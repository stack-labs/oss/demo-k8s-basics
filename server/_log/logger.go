package _log

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

func New(level string) Logger {
	return &defaultLogger{
		stdOutLogger: buildLogger(level, os.Stdout),
		stdErrLogger: buildLogger(level, os.Stderr),
	}
}

func buildLogger(level string, writer io.Writer) *logrus.Logger {
	log := logrus.New()

	switch {
	case strings.EqualFold("info", level):
		log.SetLevel(logrus.InfoLevel)
	case strings.EqualFold("warn", level):
		log.SetLevel(logrus.WarnLevel)
	case strings.EqualFold("error", level):
		log.SetLevel(logrus.ErrorLevel)
	case strings.EqualFold("fatal", level):
		log.SetLevel(logrus.FatalLevel)
	case strings.EqualFold("debug", level):
		log.SetLevel(logrus.DebugLevel)
	default:
		defer func() {
			log.Warn(fmt.Sprintf("unknown log level '%s', falling back to '%s' level", level, log.Level))
		}()
		log.SetLevel(logrus.DebugLevel)
	}
	log.Formatter = new(formatter)
	log.Out = writer

	return log
}

type Fields logrus.Fields

type Logger interface {
	Info(ctx context.Context, message string, fields Fields)
	Debug(ctx context.Context, message string, fields Fields)
	Warn(ctx context.Context, message string, fields Fields)
	Error(ctx context.Context, message string, fields Fields)
	Fatal(ctx context.Context, message string, fields Fields)
}

type defaultLogger struct {
	stdOutLogger *logrus.Logger
	stdErrLogger *logrus.Logger
}

func (l *defaultLogger) Debug(ctx context.Context, message string, fields Fields) {
	l.baseEntry(ctx, l.stdOutLogger, logrus.Fields(fields)).Debug(message)
}

func (l *defaultLogger) Info(ctx context.Context, message string, fields Fields) {
	l.baseEntry(ctx, l.stdOutLogger, logrus.Fields(fields)).Info(message)
}

func (l *defaultLogger) Warn(ctx context.Context, message string, fields Fields) {
	l.baseEntry(ctx, l.stdErrLogger, logrus.Fields(fields)).Warn(message)
}

func (l *defaultLogger) Error(ctx context.Context, message string, fields Fields) {
	l.baseEntry(ctx, l.stdErrLogger, logrus.Fields(fields)).Error(message)
}

func (l *defaultLogger) Fatal(ctx context.Context, message string, fields Fields) {
	l.baseEntry(ctx, l.stdErrLogger, logrus.Fields(fields)).Fatal(message)
}

func (l *defaultLogger) baseEntry(_ context.Context, logger *logrus.Logger, additionalFields logrus.Fields) *logrus.Entry {
	if additionalFields == nil {
		additionalFields = make(logrus.Fields)
	}

	finalFields := logrus.Fields{
		"app":               "demo-k8s-server",
		"timeHumanReadable": time.Now().UTC().String(),
		"application_date":  time.Now().UTC().Format("2006-01-02T15:04:05.000000Z"),
		"payload":           additionalFields,
		"message_type":      "APP",
	}

	return logger.WithFields(finalFields)
}
