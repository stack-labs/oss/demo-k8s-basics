package _log

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"reflect"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

type formatter struct{}

func (f formatter) Format(entry *logrus.Entry) ([]byte, error) {
	data := make(Fields)
	for k, v := range entry.Data {
		data[k] = f.formatField(v, 0)
	}

	level := strings.ToUpper(entry.Level.String())
	data["level"] = level
	data["severity"] = level
	data["time"] = entry.Time
	data["message"] = entry.Message

	serialized, err := json.Marshal(data)
	if err != nil {
		// retry marshaling without payload
		data["payload"] = map[string]interface{}{
			"jsonError":   err.Error(),
			"dataPayload": fmt.Sprintf("%+v", data["payload"]),
		}
		serialized, err = json.Marshal(data)
		if err != nil {
			return nil, err
		}
	}

	return append(serialized, '\n'), nil
}

func (f formatter) formatField(fieldValue interface{}, depth int) interface{} {
	const arbitraryMaxDepth = 100
	if depth > arbitraryMaxDepth {
		return nil
	}
	depth++

	switch value := fieldValue.(type) {
	case nil: // because otherwise, we marshal nil and return json.RawMessage([]byte("null"))
		return nil
	case logrus.Fields:
		if len(value) == 0 {
			return nil
		}
		d := make(map[string]interface{}, len(value))
		for k, v := range value {
			d[k] = f.formatField(v, depth)
		}
		return d
	case error:
		// Otherwise errors are ignored by `encoding/json`
		// https://github.com/sirupsen/logrus/issues/137
		return f.formatField(value.Error(), depth)
	case http.Request:
		return map[string]interface{}{
			"method":           value.Method,
			"url":              value.URL.String(),
			"proto":            value.Proto,
			"header":           value.Header,
			"contentLength":    value.ContentLength,
			"transferEncoding": value.TransferEncoding,
			"host":             value.Host,
			"trailer":          value.Trailer,
			"remoteAddr":       value.RemoteAddr,
			"requestUri":       value.RequestURI,
		}
	case http.Response:
		res := map[string]interface{}{
			"status":           value.Status,
			"statusCode":       value.StatusCode,
			"proto":            value.Proto,
			"header":           value.Header,
			"contentLength":    value.ContentLength,
			"transferEncoding": value.TransferEncoding,
			"uncompressed":     value.Uncompressed,
			"trailer":          value.Trailer,
		}
		if value.Request != nil {
			res["request"] = f.formatField(*value.Request, depth)
		}
		return res
	case url.URL:
		return value.String()
	case complex64, complex128:
		return fmt.Sprint(value)
	case time.Time:
		return value
	case []byte:
		return string(value)
	case json.RawMessage:
		return string(value)
	// all other cases are handled by reflection
	default:
		switch v := reflect.ValueOf(value); v.Kind() {
		case reflect.Ptr:
			ret := v.Elem()
			if v.IsNil() || !ret.CanInterface() {
				return nil
			}
			return f.formatField(ret.Interface(), depth)
		case reflect.Struct:
			var item reflect.Value
			typeOf := v.Type()
			d := make(map[string]interface{}, v.NumField())
			for i := 0; i < v.NumField(); i++ {
				item = v.Field(i)
				if !item.CanInterface() {
					continue
				} else {
					d[typeOf.Field(i).Name] = f.formatField(item.Interface(), depth)
				}
			}
			return d
		case reflect.Func, reflect.Chan:
			return fmt.Sprintf("formatter unsupported field: %v", v.Type().String())
		case reflect.Map:
			keys := v.MapKeys()
			res := make(map[string]interface{}, len(keys))
			for _, key := range keys {
				mapValue := v.MapIndex(key)
				if !key.CanInterface() || !mapValue.CanInterface() {
					continue
				}
				res[fmt.Sprintf("%v", key.Interface())] = f.formatField(mapValue.Interface(), depth)
			}
			return res
		case reflect.Slice:
			res := make([]interface{}, v.Len())
			for i := 0; i < v.Len(); i++ {
				res[i] = f.formatField(v.Index(i).Interface(), depth)
			}
			return res
		// since we handle structs, maps, ... before that, this should serialize only "leafs" and be relatively safe
		default:
			serialized, err := json.Marshal(value)
			if err != nil {
				return err.Error()
			}
			return json.RawMessage(serialized)
		}
	}
}
